import { Recipe } from "./../entities/recipe";
import { RecipeElement } from "../components/recipes";
import { Renderer } from "../renderer";
import { Store } from "../store";

export function setRecipeElements() {
  const renderer = Renderer.getInstance();
  renderer.buildRecipes();
}

export function addToTagList(tag: string, type: string) {
  const renderer = Renderer.getInstance();
  const store = Store.getInstance();
  const tags = store.getStore("tags");

  store.setStore("tags", [...new Set([...tags, tag])]);

  renderer.buildTag(tag, type);
  filterRecipes();
}

export function removeFromTagList(tag: string) {
  const store = Store.getInstance();
  const tags = store.getStore("tags");

  store.setStore(
    "tags",
    tags.filter((currentTag) => currentTag !== tag)
  );

  filterRecipes();
}

export function filterRecipes() {
  const store = Store.getInstance();

  const tags = store.getStore("tags");
  const research = store.getStore("research");

  const recipeElements = store.getStore("recipes").reduce((recipes, recipe) => {
    if (!research && !tags.length) {
      recipes.push(new RecipeElement(recipe));
      return recipes;
    }

    if (
      !!doesRecipeIncludeValue(recipe, research) &&
      !!isTagMatch(recipe, tags)
    ) {
      recipes.push(new RecipeElement(recipe));
      return recipes;
    }

    return recipes;
  }, [] as RecipeElement[]);

  store.setStore("recipeElements", recipeElements);
}

function isTagMatch(recipe: Recipe, tags: string[]) {
  return tags.every(
    (tag) =>
      recipe.ingredients.some(({ name }) => tag === name.toLowerCase()) ||
      tag === recipe.appliance.toLowerCase() ||
      recipe.ustensils.some((ustensils) => tag === ustensils.toLowerCase())
  );
}

function doesRecipeIncludeValue(recipe: Recipe, searchValue: string) {
  return (
    recipe.ingredients.some(({ name }) =>
      name.toLowerCase().includes(searchValue.toLowerCase())
    ) ||
    recipe.description.toLowerCase().includes(searchValue.toLowerCase()) ||
    recipe.name.toLowerCase().includes(searchValue.toLowerCase())
  );
}
