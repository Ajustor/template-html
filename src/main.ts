import { RECIPES } from "./data/recipes";
import { RecipeElement } from "./components/recipes";
import { Renderer } from "./renderer";
import { Store } from "./store";

const store = Store.getInstance();
store.setStore("recipes", RECIPES);

const recipeElements = store
  .getStore("recipes")
  .map((recipe) => new RecipeElement(recipe));

store.setStore("recipeElements", recipeElements);

Renderer.getInstance().buildFilters();

const searchbar = document.getElementById("search")!;

searchbar?.addEventListener("input", ({ target }: any) => {
  store.setStore("research", target.value);
});
