import { filterRecipes, setRecipeElements } from "./store/action";

import { Recipe } from "./entities/recipe";
import { RecipeElement } from "./components/recipes";

export type StoreElements = {
  recipes: Recipe[];
  recipeElements: RecipeElement[];
  tags: string[];
  research: string;
};

const actions = {
  recipes: () => null,
  recipeElements: setRecipeElements,
  tags: () => null,
  research: filterRecipes,
};

export class Store {
  private store: StoreElements;
  private static instance: Store;

  constructor() {
    this.store = new Proxy(
      {
        recipes: [],
        recipeElements: [],
        tags: [],
        research: "",
      },
      {
        set(target, key: keyof StoreElements, value) {
          target[key] = value;

          actions[key]();
          return true;
        },
      }
    );
  }

  static getInstance(): Store {
    if (!this.instance) {
      this.instance = new Store();
    }

    return this.instance;
  }

  getStore<K extends keyof StoreElements, T extends StoreElements[K]>(
    name: K
  ): T {
    return Store.getInstance().store[name] as T;
  }

  setStore<K extends keyof StoreElements, T extends StoreElements[K]>(
    name: K,
    value: T
  ) {
    Store.getInstance().store[name] = value;
  }
}
