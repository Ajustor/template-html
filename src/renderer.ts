import { Filter } from "./components/filter";
import { Store } from "./store";
import { removeFromTagList } from "./store/action";

export class Renderer {
  private static instance: Renderer;
  private store: Store = Store.getInstance();

  private constructor() {}

  static getInstance(): Renderer {
    if (!this.instance) {
      this.instance = new Renderer();
    }

    return this.instance;
  }

  buildFilters() {
    const recipeElements = this.store.getStore("recipeElements");

    const filterContainer: HTMLElement = document.getElementById("filters")!;

    const ingredientsFilter = new Filter(
      "ingredients",
      [
        ...new Set(
          recipeElements.map((recipe) => recipe.getIngredients()).flat()
        ),
      ],
      "Ingredient"
    );
    const appliancesFilter = new Filter(
      "appliances",
      [
        ...new Set(
          recipeElements.map((recipe) => recipe.getApplicance()).flat()
        ),
      ],
      "Appareils"
    );
    const ustensilsFilter = new Filter(
      "ustensils",
      [
        ...new Set(
          recipeElements.map((recipe) => recipe.getUstencils()).flat()
        ),
      ],
      "Ustensiles"
    );

    ingredientsFilter.attachTo(filterContainer);
    appliancesFilter.attachTo(filterContainer);
    ustensilsFilter.attachTo(filterContainer);
  }

  buildRecipes() {
    const recipesContainer: HTMLElement =
      document.getElementById("recipe-cards")!;

    this.clearContainer(recipesContainer);

    for (const recipeElement of this.store.getStore("recipeElements")) {
      recipeElement.attachTo(recipesContainer);
    }
  }

  buildTag(tagValue: string, type: string) {
    const tagContainer: HTMLElement =
      document.getElementById("tags-container")!;
    if (tagContainer.querySelector(`span[data-${type}="${tagValue}"]`)) {
      return;
    }
    const tag = document.createElement("span");
    const closeSpan = document.createElement("span");
    tag.dataset[type] = tagValue;
    tag.classList.add(`${type}`, "tag");
    closeSpan.innerText = "X";
    closeSpan.classList.add("cross");
    tag.innerText = tagValue;
    tag.appendChild(closeSpan);

    tagContainer.appendChild(tag);

    closeSpan.addEventListener("click", () => {
      tag.remove();
      removeFromTagList(tagValue);
    });
  }

  private clearContainer(container: HTMLElement) {
    while (container.firstChild) {
      container.removeChild(container.firstChild);
    }
  }
}
