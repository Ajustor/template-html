import { Recipe } from "../entities/recipe";

export class RecipeElement {
  private htmlElement: HTMLElement;

  constructor(private readonly recipe: Recipe) {
    const cardTemplate: HTMLTemplateElement =
      document.querySelector("template#recipe")!;
    this.htmlElement = cardTemplate.content.cloneNode(true) as HTMLElement;

    this.buildTitle();
    this.buildIngredients();
    this.buildDescription();
  }

  attachTo(parent: HTMLElement) {
    parent.appendChild(this.htmlElement);
  }

  getIngredients(): string[] {
    return this.recipe.ingredients.map(({ name }) => name.toLowerCase().trim());
  }

  getApplicance(): string {
    return this.recipe.appliance.toLowerCase().trim();
  }

  getUstencils() {
    return this.recipe.ustensils.map((ustensil) =>
      ustensil.toLowerCase().trim()
    );
  }

  private buildTitle() {
    const titleElement = this.htmlElement.querySelector("header")!;

    titleElement.querySelector("h1")!.textContent = `${this.recipe.name}`;
    titleElement.querySelector("span")!.textContent = `${this.recipe.time}min`;
  }

  private buildIngredients() {
    const ingredientsElement = this.htmlElement.querySelector(
      "div.content>ul.ingredients"
    )!;

    for (const ingredient of this.recipe.ingredients) {
      const ingredientElement = document.createElement("li");
      ingredientElement.textContent = `${ingredient.name}${
        ingredient.quantity ? `: ${ingredient.quantity}` : ""
      }${ingredient.unit ? ` ${ingredient.unit}` : ""}`;

      ingredientsElement.appendChild(ingredientElement);
    }
  }

  private buildDescription() {
    const descriptionElement = this.htmlElement.querySelector(
      "div.content>div.description"
    )!;

    descriptionElement.textContent = this.recipe.description;
  }
}
