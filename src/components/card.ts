export class Card {
  private htmlElement: HTMLElement;

  constructor(title: string, content: string) {
    const cardTemplate: HTMLTemplateElement =
      document.querySelector("template#card")!;
    this.htmlElement = cardTemplate.content.cloneNode(true) as HTMLElement;
    const titleElement = this.htmlElement.querySelector("header")!;
    const contentComponent = this.htmlElement.querySelector("span.body")!;

    titleElement.textContent = title;
    contentComponent.textContent = content;
  }

  attachTo(parent: HTMLElement) {
    parent.appendChild(this.htmlElement);
  }
}
