import { addToTagList } from "../store/action";

export class Filter {
  private htmlElement: HTMLElement;
  private currentSearchResult: string[];
  private listView!: HTMLElement;
  private input!: HTMLInputElement;
  private inputClearTimeout!: number;

  constructor(
    private readonly type: string,
    private readonly listData: string[],
    placeholder: string = ""
  ) {
    const cardTemplate: HTMLTemplateElement =
      document.querySelector("template#filter")!;
    this.htmlElement = cardTemplate.content.cloneNode(true) as HTMLElement;
    this.currentSearchResult = [...this.listData];

    this.buildInput(placeholder);
    this.buildList();
  }

  attachTo(parent: HTMLElement) {
    parent.appendChild(this.htmlElement);
  }

  private buildInput(placeholder: string) {
    this.input = this.htmlElement.querySelector(`input[type="search"]`)!;

    this.input.classList.add(this.type);
    this.input.id = this.type;
    this.input.placeholder = placeholder;

    this.input.addEventListener("input", ({ target }: any) => {
      this.currentSearchResult = this.listData.filter((value) =>
        value.toLowerCase().includes(target.value)
      );
      this.updateList();
    });

    this.input.addEventListener("blur", () => {
      this.inputClearTimeout = setTimeout(() => {
        this.hideListView();
        this.input.value = "";
      }, 100);
    });

    this.input.addEventListener("click", () => {
      this.updateList();
      this.showListView();
    });
  }

  private buildList() {
    this.listView = this.htmlElement.querySelector(`ul.list`)!;

    this.listView.classList.add(this.type);
    this.listView.id = `${this.type}-list`;
  }

  private updateList() {
    this.clearList();

    for (const element of this.currentSearchResult) {
      const clickableSpan = document.createElement("li");
      clickableSpan.textContent = element;
      clickableSpan.addEventListener("click", (e) => {
        e.preventDefault();
        addToTagList(element, this.type);
        if (this.inputClearTimeout) {
          clearTimeout(this.inputClearTimeout);
        }
        this.input.focus({
          preventScroll: true,
        });
      });
      this.listView.appendChild(clickableSpan);
    }
  }

  private clearList() {
    while (this.listView.firstChild) {
      this.listView.removeChild(this.listView.firstChild);
    }
  }

  private hideListView() {
    this.listView.dataset.hidden = "true";
  }

  private showListView() {
    this.listView.dataset.hidden = "false";
  }
}
